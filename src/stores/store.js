import { defineStore } from 'pinia';
import data from '/src/data/quizz.json'; // Adjust the path

export const useQuestionStore = defineStore('question', {
  state: () => ({
    questions: new Array({id: 1, theme:"climat", q : "Le rechauffement climatique n'existe pas", res: false, explication : "<b>test</b>"}, {id: 1, q : "c'est vrai", res: true, explication : "<b>test</b>"}),
    jsonData: data,
  }),
  getters: {
  },
  actions: {
  },
});
export const JsonData = defineStore('jsondata', {
  state: () => ({
    jsonData: data,
  }),
  getters: {
    getJsonData: state => state.jsonData
  },
  actions: {
  },
});
